## Version 1.2
- New input feature: Enter compunds through text input (including subscript and parenthesis)!
- Dark UI
- Changed how some numbers are stored to increase precision
- Added service worker and manifest for offline availability
- Input verification for the new input method
- Switched away from G-Fonts
- Added the license file and disclaimers
- Favicon
- Footer
- Changed keyboard buttons' functionality
- Added changelog
- Major compatibility improvements for older versions of Firefox and other browsers.
- LOTS of bug fixes, UI tweaks and other minor improvements to the functionality.

## Version 1.1
- Atomic numbers
- Removed greymode
- Separate javascript file
- Updated some standard atomic weights
- Added AtomicWeightDisclaimer.html
- Made the elements into actual buttons instead of just being clickable
- Added animations
- Added clear button
- 

## Version 1.0
- Full periodic table
- Colored elements
- Significant figures selector
- Removal of last added element
- Legend over element colors
- Keyboard buttons for functionality
- Progress made on scalability
- Improved formula handling system
- element.json file with atomic weights
- Greymode
