const staticCacheName = "staticResources-v23"
//<const dynamicCacheName = "dynamicResources"
const assets = [
    "./",
    "./index.html",
    "./pages/AtomicWeightDisclaimer.html",
    "./pages/fallback.html",
    "./js/javascript.js",
    "./styles.css",
    "./js/decimal.min.js",
    "./data/elements.json",
    "./fonts/cutive-mono-v8-latin-ext_latin-regular.eot",
    "./fonts/cutive-mono-v8-latin-ext_latin-regular.svg",
    "./fonts/cutive-mono-v8-latin-ext_latin-regular.ttf",
    "./fonts/cutive-mono-v8-latin-ext_latin-regular.woff",
    "./fonts/cutive-mono-v8-latin-ext_latin-regular.woff2",
    "./fonts/CutiveMono-Regular.ttf",
    "./icons/favicon.ico",
    "./icons/favicon-16x16.png",
    "./icons/favicon-32x32.png"
];



//INSTALL SERVICE WORKER
self.addEventListener("install", evt => {
    //console.log("[Service Worker] Installed");
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
            console.log("[Service Worker] Caching assets");
            cache.addAll(assets);
        })
    )
});

//ACTIVATE SERVICE WORKER
self.addEventListener("activate", evt => {
    //console.log("[Service Worker] Activated");
    evt.waitUntil(
        caches.keys().then(keys => {
            //console.log(keys);
            return Promise.all(keys
                .filter(key => key !== staticCacheName/* && key !== dynamicCacheName*/)
                .map(key => caches.delete(key))
            )
        })
    )
});

//FETCH EVENT
self.addEventListener("fetch", evt => {
    console.log("[Service Worker] Fetched", evt.request.url);
    evt.respondWith(
        caches.match(evt.request.url).then(cacheResponse => {
            if (cacheResponse == undefined) {
                //console.log("[Service Worker] Not found in cache ", evt.request.url);
                //console.log("[Service Worker] PromiseValue: undefined means it was not found in the cache ", caches.match(evt.request.url));
            }
            return cacheResponse || fetch(evt.request.url)/*.then(fetchResponse => {        For use when there are multiple (uncached) pages
                return caches.open(dynamicCacheName).then(cache => {
                    cache.put(evt.request.url, fetchResponse.clone());
                    limitCacheSize(dynamicCacheName, 20);
                    return fetchResponse;
                })
            });*/
        }).catch(() => {
            if(evt.request.url.indexOf(".html") > -1){
                return caches.match("/pages/fallback.html");
            }
        })
    )
});

//CACHE SIZE LIMITER
const limitCacheSize = (name, size) => {
    caches.open(name).then(cache => {
        cache.keys().then(keys => {
            if(keys.length > size){
                cache.delete(keys[0]).then(limitCacheSize(name, size))
            }
        })
    })
}