## MolarCalc
### Interactive Periodic Table for Molar Masses
MolarCalc aims to ease in the monotonous addition of molar masses by allowing users to either click on elements to add their masses or, in a thus far rather limited way, input the compounds using symbols, subscript (not typed as such) and parenthesis.

The website is available at [molarcalc.com](https://molarcalc.com)